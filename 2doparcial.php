<?php

define('tamanomat', 4);

function generarMatrizAleatoria($tamano) {
    $matriz = array();

    for ($i = 0; $i < $tamano; $i++) {
        for ($j = 0; $j < $tamano; $j++) {
            $matriz[$i][$j] = rand(0, 9);
        }
    }

    return $matriz;
}

function imprimirMatriz($matriz) {
    echo "<table border='1'>";
    foreach ($matriz as $fila) {
        echo "<tr>";
        foreach ($fila as $valor) {
            echo "<td>$valor</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
}

function sumaDiagonalPrincipal($matriz) {
    $suma = 0;
    $tamano = count($matriz);

    for ($i = 0; $i < $tamano; $i++) {
        $suma += $matriz[$i][$i];
    }

    return $suma;
}

do {
    $matriz = generarMatrizAleatoria(tamanomat);
    $sumaDiagonal = sumaDiagonalPrincipal($matriz);

    echo "Matriz generada:<br>";
    imprimirMatriz($matriz);
    echo "Suma de la diagonal principal: $sumaDiagonal<br><br>";

    if ($sumaDiagonal >= 10 && $sumaDiagonal <= 15) {
        echo "La suma de la diagonal principal está entre 10 y 15.";
        break;
    }
} while (true);
?>
